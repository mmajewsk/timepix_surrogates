# -*- coding: utf-8 -*-
# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from common import make_data_dict, make_sur_map, make_bin_map, make_sur, myscore
from common import cut_range, clean_parameters, datafiles, devices, plot_all_hist_params

# %%
data_folder = Path("./data/sur/")


paired_data = list(zip(datafiles[::2], datafiles[1::2]))

sensor_data = {}
for before, post in paired_data:
    sensor = before.split('_')[0]
    df_b = pd.read_csv(data_folder/before, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
    df_p = pd.read_csv(data_folder/post, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
    sensor_data[sensor] = (df_b, df_p)

from scipy import stats

def plot_parameter_histos(sensor_name, pre, post, axe, ranges, xfontsize=10, yfontsize=10):
    p0_range, p1_range, c_range, t_range = ranges
    pre2 = clean_parameters(pre)[['p0','p1','c','t']]
    plot_all_hist_params(pre2, axe, ranges, label='pre')
    post2= clean_parameters(post)[['p0','p1','c','t']]
    axe[0].set_ylabel(sensor_name, fontsize=yfontsize)
    plot_all_hist_params(post2, axe, ranges, label='post')
    for j, param in zip(range(4), ['p0','p1','c','t']):
        axe[j].set_xlabel(param, fontsize=xfontsize)
    axe[-1].legend()
    return axe

def get_all_histos(sensor_data):
    p0_range=(-10,10)
    p1_range=(0,0.010)
    c_range=(0,15000)
    t_range=(-2000,3000)
    ranges = p0_range, p1_range, c_range, t_range
    fig, axes = plt.subplots(len(sensor_data),4,figsize=(18,40))
    for i, (sensor_name, (pre, post)) in enumerate(sensor_data.items()):
        axe_ = plot_parameter_histos(sensor_name, pre, post, axes[i], ranges)
    return fig

def get_single_detectors(sensor_data, subplot_args, xfontsize=10, yfontsize=10):
    p0_range=(-10,10)
    p1_range=(0,0.010)
    c_range=(0,15000)
    t_range=(-2000,3000)
    ranges = p0_range, p1_range, c_range, t_range
    figurs = {}
    for i, (sensor_name, (pre, post)) in enumerate(sensor_data.items()):
        fig, axes = plt.subplots(1,4,**subplots_args)
        axe_ = plot_parameter_histos(sensor_name, pre, post, axes, ranges, xfontsize, yfontsize)
        fig.tight_layout()
        figurs[sensor_name] = fig
    return figurs

def plot_all_histos(sensor_data):
    fig = get_all_histos(sensor_data)
    fig.tight_layout()
    fig.show()

from scipy.stats import pearsonr
import seaborn as sns
import matplotlib.pyplot as plt

def corrfunc(x,y, ax=None, **kws):
    """Plot the correlation coefficient in the top left hand corner of a plot."""
    r, pval = pearsonr(x, y)
    ax = ax or plt.gca()
    # Unicode for lowercase rho (ρ)
    rho = '\u03C1'
    ax.annotate(f'{rho} = {r:.2f} pval = {pval}', xy=(.1, .9), xycoords=ax.transAxes)

def plot_corr_matrix(corrMatrix, data):
    plt.figure()
    sns.set(font_scale=1)
    sns_plot = sns.heatmap(corrMatrix, annot=True).set_title(title)
    fig = sns_plot.get_figure()
    fig.tight_layout()
    return fig

def plot_corr_dots(d, title):
    sns.set(font_scale=2)
    g = sns.pairplot(d)
    g.fig.suptitle(title, y=1.08)
    # g.map_lower(corrfunc)
    fig = g.fig
    fig.tight_layout()
    return fig


def get_both(data, title):
    data = data.dropna()
    data2 = clean_parameters(data)[['p0','p1','c','t']]
    d = data2[['p0','p1','c','t']]
    corrMatrix = d.corr()
    fig1 = plot_corr_matrix(corrMatrix, title)
    fig2 = plot_corr_dots(d, title)
    return fig1, fig2


def plotboth(data, title):
    fig1, fig2 = get_both(data, title)
    fig1.show()
    fig2.show()

def plot_correlations(sensor_data):
    for sensor_name, (pre, post) in sensor_data.items():
        title = "{} {}".format(sensor_name, 'pre')
        plotboth(pre, title)
        title = "{} {}".format(sensor_name, 'post')
        plotboth(post, title)


if __name__=="__main__":
    # use those two to plot all of the stuff
    # plot_all_histos(sensor_data)
    # plot_correlations(sensor_data)

    plot_save_path = Path('plots_presentation/')
    subplots_args = dict(figsize=(15,4))
    figures_dict = get_single_detectors(sensor_data, subplots_args, xfontsize=14, yfontsize=18)
    # plt.rcParams.update({'font.size': 32})
    for sensor_name, fig in figures_dict.items():
        filename = "p1_{}_histos.png".format(sensor_name)
        fig.savefig(plot_save_path/filename)


    sensor_name = 'S8'
    pre, post = sensor_data[sensor_name]

    title = "{} {}".format(sensor_name, 'pre')
    fig1, fig2 = get_both(pre, title)
    filename = "p1_{}_dots_pre.png".format(sensor_name)
    fig2.savefig(plot_save_path/filename)
    filename = "p1_{}_corr_pre.png".format(sensor_name)
    fig1.savefig(plot_save_path/filename)


    title = "{} {}".format(sensor_name, 'post')
    fig1, fig2 = get_both(post, title)
    filename = "p1_{}_dots_post.png".format(sensor_name)
    fig2.savefig(plot_save_path/filename)
    filename = "p1_{}_corr_post.png".format(sensor_name)
    fig1.savefig(plot_save_path/filename)
