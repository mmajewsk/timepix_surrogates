# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from common import make_data_dict, make_sur_map, make_bin_map, make_sur, myscore, cut_range, clean_parameters
from collections import OrderedDict
from scipy.optimize import curve_fit
import seaborn as sns
from pathlib import Path


sensor_data = make_data_dict()
pre_map =  make_sur_map(sensor_data['S8'][0])
post_map =  make_sur_map(sensor_data['S8'][1])
BINNING_TYPE = '8'
df2 = pd.read_csv("data/bin_map/EllipticIRRAD{}.csv".format(BINNING_TYPE), skiprows=1, names=['col','row','bin'], sep=" ")
bin_map = make_bin_map(df2)
binsize = len(df2['bin'].unique())
pre = pd.merge(sensor_data['S8'][0], df2, on=['col','row'])
post = pd.merge(sensor_data['S8'][1], df2, on=['col','row'])

bin_8_fluences = [
7.504e+15 ,
7.187e+15,
6.595e+15,
5.76e+15,
4.766e+15,
3.655e+15,
2.681e+15,
1.779e+15 ,
]

from common import SuroGen

def plot_just_cut(pre_cut, axes):
    for m in range(4):
        axes[m].hist(pre_cut.values[:,m],bins=100)
    return axes

def produce_tmp_bins(pre_cut, axes):
    tmpbins = []
    for m in range(4):
        _, bins, _  = axes[m].hist(pre_cut.values[:,m],bins=50, label="raw data", alpha=0.5)
        tmpbins.append(bins)
    return tmpbins

def generated_original(decorelated, ax, GENSIZE):
    # axes = ax[2]
    # _ = axes[0].hist(decorelated[:,0], bins=100, label="original (n={})".format(GENSIZE))
    # _ = axes[1].hist(decorelated[:,1], bins=100, label="original (n={})".format(GENSIZE))
    # _ = axes[2].hist(decorelated[:,2], bins=100, label="original (n={})".format(GENSIZE))
    # _ = axes[3].hist(decorelated[:,3], bins=100, label="original (n={})".format(GENSIZE))

    axes = ax[1]
    _ = axes[0].hist(decorelated[:,0], bins=100, density=True)
    _ = axes[1].hist(decorelated[:,1], bins=100, density=True)
    _ = axes[2].hist(decorelated[:,2], bins=100, density=True)
    _ = axes[3].hist(decorelated[:,3], bins=100, density=True)

def generated2(sgn, decorelated, ax):
    EXAMPLE_GENSIZE = 35000
    exmpl = sgn.generate_decor(EXAMPLE_GENSIZE)
    for j in range(4):
        data = decorelated[:,j]
        params = list(sgn.fit_params.items())[j][1]
        size = 1000
        x = np.linspace(data.min(), data.max(), size)
        pdf_fitted = sgn.dist.pdf(x, *params)
        axes = ax[1]
        axes[j].plot(x, pdf_fitted)
        # = dist.rvs(GENSIZE)
        example_gen = exmpl[:,j]
        # axes = ax[2]
        # axes[j].hist(example_gen, bins=100, label="generated samples (n={})".format(EXAMPLE_GENSIZE))
        # axes[j].legend()

def recor_make(recor, tmpbins, axes):
    for m in range(4):
        axes[m].hist(recor[:,m],bins=tmpbins[m], label="generated", alpha=0.5)
        axes[m].legend()

# def add_labels(ax):
#     ax[0,0].set_ylabel("raw data")
#     ax[2,0].set_ylabel("generated")
#     ax[1,0].set_ylabel("rescaled, decoralted and fitted")
#     ax[3,0].set_ylabel("raw and generated")
#     ax[3,0].set_xlabel("p0")
#     ax[3,1].set_xlabel("p1")
#     ax[3,2].set_xlabel("c")
#     ax[3,3].set_xlabel("t")


def add_labels(ax):
    ax[0,0].set_ylabel("raw data")
    ax[1,0].set_ylabel("rescaled, decoralted and fitted")
    ax[2,0].set_ylabel("raw and generated")
    ax[2,0].set_xlabel("p0")
    ax[2,1].set_xlabel("p1")
    ax[2,2].set_xlabel("c")
    ax[2,3].set_xlabel("t")

def model_pipe(pre_cut, sgn, GENSIZE, recor):
    fig, ax = plt.subplots(3,4, figsize=(16,8))
    ax[0] = plot_just_cut(pre_cut, ax[0])
    tmpbins = produce_tmp_bins(pre_cut, ax[2])
    decorelated = sgn.generate_decor(GENSIZE)
    generated_original(decorelated, ax, GENSIZE)
    generated2(sgn, decorelated, ax)
    recor_make(recor, tmpbins, ax[2])
    add_labels(ax)
    fig.tight_layout()
    return fig, decorelated

def cor_matrix_post_pca(decorelated):
    plt.figure()
    corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
    sns.heatmap(corrMatrix, annot=True)
    plt.show()


def generate_histos(data):
    genmeans = []
    genstd = []
    gmeans_dict = {}
    surogen_in_bin = {}
    generated_all = []
    for i, (name, group) in enumerate(data.groupby('bin')):
        surogen = SuroGen()
        surogen.fit(group)
        rmi = surogen.means()[0]
        genstd.append(surogen.scaler.scale_)
        genmeans.append(rmi)
        generated = surogen.generate(group.shape[0])
        generated_all.append(generated)
        surogen_in_bin[name] = surogen
        gmeans_dict[i] = rmi
    return genmeans, genstd, generated_all, gmeans_dict

def histos_bins_pre(generated_all):
    figs = []
    maxs = [[],[],[],[]]
    mins = [[],[],[],[]]

    for i, (name, group) in enumerate(pre.groupby('bin')):
        generated = generated_all[i]
        for y_ in range(4):
            mins[y_].append(generated[:, y_].min())
            maxs[y_].append(generated[:, y_].max())
    realmax = [max(k) for k in maxs]
    realmin = [min(k) for k in mins]
    ranges = list(zip(realmax, realmin))


    for i, (name, group) in enumerate(pre.groupby('bin')):
        fig, axes = plt.subplots(1,4, figsize=(12,2))
        pre_params = group[['p0','p1','c','t']]
        pre_cut_bin = cut_range(pre_params)
        generated = generated_all[i]
        _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
        _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
        _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
        _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

        axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
        axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
        axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
        axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
        axes[0].set_ylabel("Bin {}".format(name))

        axes[0].set_xlabel('p0')
        axes[1].set_xlabel('p1')
        axes[2].set_xlabel('c')
        axes[3].set_xlabel('t')
        for y_ in range(4):
            axes[y_].set_xlim(ranges[y_])
            axes[y_].grid()
        axes[3].legend()
        fig.tight_layout()
        figs.append(fig)
    return figs

def histos_bin_post(genmeans_pre, genstd_pre, generated_all, gmeans_dict ):
    #generated_to_save_all = {}
    #params_in_bin = {}
    post_surogen = {}

    figs = []

    maxs = [[],[],[],[]]
    mins = [[],[],[],[]]

    for i, (name, group) in enumerate(pre.groupby('bin')):
        generated = generated_all[i]
        for y_ in range(4):
            mins[y_].append(generated[:, y_].min())
            maxs[y_].append(generated[:, y_].max())
    realmax = [max(k) for k in maxs]
    realmin = [min(k) for k in mins]
    ranges = list(zip(realmax, realmin))
    # fig.suptitle("S8 post generated vs and original distribution per bin", fontsize=24)

        #print([v for v in surogen.fit_params.values()])
        #print(rmi)
    for i, (name, group) in enumerate(post.groupby('bin')):
        fig, axes = plt.subplots(1,4, figsize=(12,2))
        pre_params = group[['p0','p1','c','t']]
        pre_cut_bin = cut_range(pre_params)
        rmi = gmeans_dict[i]
        # axes[0].axvline(rmi[0])
        # axes[1].axvline(rmi[1])
        # axes[2].axvline(rmi[2])
        # axes[3].axvline(rmi[3])

        _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
        _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
        _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
        _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

        generated = generated_all[i]
        axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
        axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
        axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
        axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
        axes[0].set_ylabel("Bin {}".format(name))

        for y_ in range(4):
            axes[y_].set_xlim(ranges[y_])
            axes[y_].grid()

        axes[3].legend()
        axes[0].set_xlabel('p0')
        axes[1].set_xlabel('p1')
        axes[2].set_xlabel('c')
        axes[3].set_xlabel('t')

        fig.tight_layout()
        figs.append(fig)
    return figs

def post_gen_dist(all_gen_arr, post_cut):
    fig, ax = plt.subplots(1,4, figsize=(16,4))
    fig.suptitle("S8 post generated vs and original distribution on whole sensor", fontsize=24)
    axes = ax
    for j in range(4):
        _, b, _ = axes[j].hist(post_cut.values[:,j], bins=100, alpha=0.6, label='raw')
        axes[j].hist(all_gen_arr[:,j], bins=b, alpha=0.6, label='generated')
    axes[0].set_xlabel("p0")
    axes[1].set_xlabel("p1")
    axes[2].set_xlabel("c")
    axes[3].set_xlabel("t")
    axes[3].legend()
    return fig

def legend_without_duplicate_labels(ax, **legend_args):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique), **legend_args)

def plot_surrogate_func(ax, row, x, **plot_args):
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    ax.plot(xfilt,y, **plot_args)
    return xfilt


def pre_gen_sur(pre_cut, recor):
    fig, axes = plt.subplots(1,1,figsize=(18,8), sharey=True)
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    axe = axes
    sampled_pre = pre_cut.sample(300)
    for i,row in sampled_pre.iterrows():
        xfilt = plot_surrogate_func(axe, row, x, color='blue', alpha=0.2, label='original')
    recor_sampled = pd.DataFrame(data=recor, index=None, columns=['p0','p1','c','t']).sample(300)
    for i,row in recor_sampled.iterrows():
        xfilt = plot_surrogate_func(axe, row, x, color='orange', alpha=0.2, label='generated')
    legend_without_duplicate_labels(axe, prop={'size': 10})
    # axe.legend()
    axe.set_ylim(bottom=0)
    return fig, xfilt


def multi_post_gen_sur(cut_data, generated, color_a='blue', label_a='original', color_b='orange', label_b='generated', resample_a=None, resample_b=None):
    #plt.style.use("bmh")
    fig, axes = plt.subplots(1,1,figsize=(10,5), sharey=True)
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    axe = axes
    if not isinstance(cut_data, pd.DataFrame):
        cut_data = pd.DataFrame(data=cut_data, index=None, columns=['p0','p1','c','t'])
    if resample_a is not None:
        sampled_pre = cut_data.sample(resample_a)
    for i,row in sampled_pre.iterrows():
        xfilt = plot_surrogate_func(axe, row, x, color=color_a, alpha=0.2, label=label_a)
    if not isinstance(generated, pd.DataFrame):
        generated = pd.DataFrame(data=generated, index=None, columns=['p0','p1','c','t'])
    if resample_b is not None:
        generated = generated.sample(resample_b)
    for i,row in generated.iterrows():
        xfilt = plot_surrogate_func(axe, row, x, color=color_b, alpha=0.2, label=label_b)
    legend_without_duplicate_labels(axe, prop={'size': 10})
    axe.set_ylim(bottom=0)
    axe.grid()
    return fig, xfilt

def bin_fitted(post, recor_sampled):
    #10.style.use("bmh")
    import matplotlib
    cmap = matplotlib.cm.get_cmap('Set1')
    fig, axes = plt.subplots(8,1,figsize=(12,18), sharey=True)
    fig2, axes2 = plt.subplots(1,1,figsize=(18,8), sharey=True)
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    fig.suptitle("Original (n=300) and generated (n=7) surrogates, S8 post - generated per bin", fontsize=16)
    # sampled_pre = post_cut.sample(30)
    # for i,row in sampled_pre.iterrows():
    #     xfilt = plot_surrogate_func(axe, row, x, color='blue', alpha=0.2, label='original')
    #
    post_cut = cut_range(post)
    for name, group in post_cut.groupby('bin'):
        axe = axes[name]
        margs = group.mean()
        # sur = make_sur(margs['p0'], margs['p1'], margs['c'], margs['t'])
        margs = group.sample(50)
        for i, row in margs.iterrows():
            xfilt = plot_surrogate_func(axe, row, x, alpha=0.1, color="blue")
            plot_surrogate_func(axes, row, x, alpha=0.1, color=cmap(i))
            # axe.plot(x, , alpha=0.2, label='post '+str(name))
    for i,row in recor_sampled.iterrows():
        axe = axes[i]
        xfilt = plot_surrogate_func(axe, row, x, alpha=0.9, linewidth=2, label=str(i), color='orange')
    legend_without_duplicate_labels(axe,prop={'size': 10})
    return fig, fig2

def new_lims(error, ybot, ytop, scale=3):
    ymean = np.mean([ybot, ytop])
    merror = np.max(error)
    newtop = ymean + scale*merror
    newbot = ymean - scale*merror
    return newbot, newtop

def fluence_fitted(recor_sampled, recor_sampled_std):

    fig,axes = plt.subplots(1,4, figsize=(16,8))
    for i, p in enumerate(SuroGen._columns):
        print(recor_sampled[p].values)
        print(recor_sampled_std[p].values)
        axes[i].plot(range(8), recor_sampled[p].values, label=p)
        error = recor_sampled_std[p].values
        (_, caps, _) = axes[i].errorbar(range(8), recor_sampled[p].values, yerr=error, label=p, fmt='.k', capsize=3 )
        for cap in caps:
            cap.set_markeredgewidth(1)

        zfit = np.polyfit(range(8),recor_sampled[p],3)
        poly = np.poly1d(zfit)
        axes[i].plot(range(8), poly(range(8)))


        ybot, ytop = axes[i].get_ylim()
        newbot, newtop = new_lims(error, ybot, ytop)
        axes[i].set_ylim(newbot, newtop)

        axes[i].set_xlabel(p)
        axes[0].set_ylabel("Vs bin")
        axes[0].set_ylabel("Vs bin fluence")
    fig.tight_layout()
    return fig

def widden_fluences(recor_sampled, record_sampled_std):

    label_error = None
    label_fit= None
    label= None
    fig,axes = plt.subplots(1,4, figsize=(16,4))
    for i, p in enumerate(SuroGen._columns):
        print(recor_sampled_std[p].values)
        if i == len(SuroGen._columns)-1:
            label_error = "crystall ball distribution per bin"
            label_fit= 'fit'
            label='crystall ball mean'
        # axes[i].plot( bin_8_fluences,recor_sampled[p].values, label=p)
        error = recor_sampled_std[p].values
        (_, caps, _) = axes[i].errorbar(bin_8_fluences, recor_sampled[p].values, yerr=error, label=label_error, fmt='.k', capsize=3 )
        ybot, ytop = axes[i].get_ylim()
        newbot, newtop = new_lims(error, ybot, ytop)
        axes[i].set_ylim(newbot, newtop)

        fitrange = np.linspace(min(bin_8_fluences), max(bin_8_fluences), 100)
        zfit = np.polyfit(bin_8_fluences,recor_sampled[p],3, w=1/recor_sampled_std[p])
        poly = np.poly1d(zfit)
        axes[i].plot(fitrange, poly(fitrange), label='fit')
        axes[i].set_xlabel(p)

    axes[-1].legend()
    fig.tight_layout()
    return fig



def pre_post_fluences_old(genmeans_pre, genstd_pre, recor_sampled, recor_sampled_std):
    recor_sampled_pre = pd.DataFrame(data=genmeans_pre, index=None, columns=['p0','p1','c','t'])
    recor_sampled_std_post = pd.DataFrame(data=genstd_pre, index=None, columns=['p0','p1','c','t'])
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    fig,axes = plt.subplots(2,4, figsize=(16,4))
    for i, p in enumerate(SuroGen._columns):
        axes[0][i].plot(bin_8_fluences, recor_sampled_pre[p].values)
        axes[1][i].plot(bin_8_fluences, recor_sampled[p].values)
        error = recor_sampled_std[p].values
        error_pre = recor_sampled_std[p].values
        (_, caps, _) = axes[1][i].errorbar(bin_8_fluences, recor_sampled[p].values, yerr=error, label=p, fmt='.k', capsize=3 )
        (_, caps, _) = axes[0][i].errorbar(bin_8_fluences, recor_sampled_pre[p].values, yerr=error_pre, label=p, fmt='.k', capsize=3 )
        diff = recor_sampled[p].values - recor_sampled_pre[p].values
        axes[0][0].set_ylabel('pre')
        axes[1][0].set_ylabel('post')
        ybot, ytop = axes[1][i].get_ylim()
        newbot, newtop = new_lims(error, ybot, ytop)
        axes[1][i].set_ylim(newbot, newtop)
        ybot, ytop = axes[0][i].get_ylim()
        newbot, newtop = new_lims(error_pre, ybot, ytop)
        axes[0][i].set_ylim(newbot, newtop)
        axes[i].set_xlabel(p+" fluence $[1e^{15} * n_{eq}/cm^{2}]$")
    fig.tight_layout()
    return fig

def pre_post_fluences(genmeans_pre, genstd_pre, recor_sampled, recor_sampled_std):
    recor_sampled_pre = pd.DataFrame(data=genmeans_pre, index=None, columns=['p0','p1','c','t'])
    recor_sampled_std_post = pd.DataFrame(data=genstd_pre, index=None, columns=['p0','p1','c','t'])
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    fig,axes = plt.subplots(1,4, figsize=(16,4))
    label_error = None
    label_fit= None
    label= None
    for i, p in enumerate(SuroGen._columns):
        # axes[0][i].plot(bin_8_fluences, recor_sampled_pre[p].values)
        # axes[1][i].plot(bin_8_fluences, recor_sampled[p].values)
        error = recor_sampled_std[p].values
        error_pre = recor_sampled_std[p].values
        # (_, caps, _) = axes[0][i].errorbar(bin_8_fluences, recor_sampled_pre[p].values, yerr=error_pre, label=p, fmt='.k', capsize=3 )
        # axes[0][0].set_ylabel('pre')
        # ybot, ytop = axes[0][i].get_ylim()
        # newbot, newtop = new_lims(error_pre, ybot, ytop)
        # axes[0][i].set_ylim(newbot, newtop)


        if i == len(SuroGen._columns)-1:
            label_error = "mean and standard"
            label_fit= 'fit'
            label='crystall ball mean'
        # axes[i].plot( bin_8_fluences,recor_sampled[p].values, label=p)
        error = recor_sampled_std[p].values
        (_, caps, _) = axes[i].errorbar(bin_8_fluences, recor_sampled[p].values, yerr=error, label=label_error, fmt='.k', capsize=3 )

        fitrange = np.linspace(min(bin_8_fluences), max(bin_8_fluences), 100)
        zfit = np.polyfit(bin_8_fluences,recor_sampled[p],2, w=1/recor_sampled_std[p])
        print(f"Fit {p}: {zfit}")
        poly = np.poly1d(zfit)
        axes[i].plot(fitrange, poly(fitrange), label='polynomial fit - 3 deg')
        axes[i].set_ylabel(p)
        axes[i].set_xlabel("fluence $[1e^{15} * n_{eq}/cm^{2}]$")
        axes[i].grid()
        ybot, ytop = axes[i].get_ylim()
        newbot, newtop = new_lims(error, ybot, ytop)
        axes[i].set_ylim(newbot, newtop)

    axes[-1].legend()
    fig.tight_layout()
    return fig


def pre_post_same_axis(genmeans_post, genstd_post, recor_sampled, recor_sampled_std):
    recor_sampled_post = pd.DataFrame(data=genmeans_post, index=None, columns=['p0','p1','c','t'])
    recor_sampled_std_post = pd.DataFrame(data=genstd_post, index=None, columns=['p0','p1','c','t'])
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 100)
    for i,row in recor_sampled_post.iterrows():
        sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
        xfilt = x[x>row['t']]
        y = sur(xfilt)
        xfilt = xfilt[y>-50]
        y = y[y>-50]
    fig,axes = plt.subplots(1,4, figsize=(16,4))
    for i, p in enumerate(SuroGen._columns):
        s = (bin_map == i).sum()
        axes[i].plot(bin_8_fluences, recor_sampled_post[p].values, label='post')
        axes[i].plot(bin_8_fluences, recor_sampled[p].values, label='pre')
        (_, caps, _) = axes[i].errorbar(bin_8_fluences, recor_sampled[p].values, yerr=recor_sampled_std[p].values/np.sqrt(s), fmt='.k', capsize=3 )
        (_, caps, _) = axes[i].errorbar(bin_8_fluences, recor_sampled_post[p].values, yerr=recor_sampled_std_post[p].values/np.sqrt(s), fmt='.k', capsize=3 )
        diff = recor_sampled[p].values - recor_sampled_post[p].values
        axes[i].set_xlabel(p+" fluence $[1e^{15} * n_{eq}/cm^{2}]$")
    axes[-1].legend()
    fig.tight_layout()
    return fig


# %%
if __name__ == "__main__":

    pre_cut = SuroGen.prepare_data(pre)
    sgn = SuroGen()
    sgn.fit(pre)
    GENSIZE = pre.shape[0]
    recor = sgn.generate(GENSIZE)


    post_cut = post[['p0','p1','c','t']]
    post_cut = cut_range(post_cut)

    plot_save_path = Path('plots_presentation/')
    fig, decorelated = model_pipe(pre_cut, sgn, GENSIZE, recor)
    filename = "p3_model_pipe.png"
    fig.savefig(plot_save_path/filename)

    genmeans_pre, genstd_pre, generated_list, _  = generate_histos(pre)

    filename = "p3_histos_pre.png"
    figs = histos_bins_pre(generated_list)
    for i, fig in enumerate(figs):
        filename = "p3_histos_pre_{}.png".format(i)
        fig.savefig(plot_save_path/filename)

    histos_post = generate_histos(post)
    genmeans_post, genstd_post, generated_list_post, gmeans_dict = histos_post

    figs = histos_bin_post(*histos_post)
    for i, fig in enumerate(figs):
        filename = "p3_histos_post_{}.png".format(i)
        fig.savefig(plot_save_path/filename)

    # # cor_matrix_post_pca(decorelated)
    # #
    all_gen_arr = np.concatenate(generated_list_post)
    # fig = post_gen_dist(all_gen_arr, post_cut)
    # filename = "p3_gen_dist_post.png"
    # fig.savefig(plot_save_path/filename)

    # #@TODO check all_gen_arr if it should be here
    fig, xfilt = multi_post_gen_sur(pre_cut, np.concatenate(generated_list), resample_a=300, resample_b=300)
    fig.suptitle("Original (n=300) and generated (n=300) surrogates S8 pre - generated for whole sensor", fontsize=16)
    filename = "p3_gen_sur_pre.png"
    fig.savefig(plot_save_path/filename)

    fig, xfilt = multi_post_gen_sur(post_cut, all_gen_arr, resample_a=300, resample_b=300)
    fig.suptitle("Original (n=300) and generated (n=300) surrogates, S8 post - generated per bin", fontsize=16)
    filename = "p3_gen_sur_post.png"
    fig.savefig(plot_save_path/filename)

    fig, xfilt = multi_post_gen_sur(np.concatenate(generated_list), all_gen_arr, label_a='pre', label_b='post', resample_a=300, resample_b=300)
    fig.suptitle("Generate pre and post (n=300) surrogates", fontsize=16)
    filename = "p3_gen_sur.png"
    fig.savefig(plot_save_path/filename)

    fig, xfilt = multi_post_gen_sur(pre_cut, post_cut, label_a='pre', label_b='post', resample_a=300, resample_b=300)
    fig.suptitle("Original pre and post (n=300) surrogates", fontsize=16)
    filename = "p3_orig_sur.png"
    fig.savefig(plot_save_path/filename)

    recor_sampled = pd.DataFrame(data=genmeans_post, index=None, columns=['p0','p1','c','t'])
    recor_sampled_std = pd.DataFrame(data=genstd_post, index=None, columns=['p0','p1','c','t'])
    # fig = bin_fitted(post, recor_sampled)
    # filename = "p3_bin_fitted.png"
    # fig.savefig(plot_save_path/filename)

    # fig = fluence_fitted(recor_sampled, recor_sampled_std)
    # filename = "p3_fluence_fitted.png"
    # fig.savefig(plot_save_path/filename)

    fig = widden_fluences(recor_sampled, recor_sampled_std)
    filename = "p3_wide_fluence_fitted.png"
    fig.savefig(plot_save_path/filename)

    fig = pre_post_same_axis(genmeans_post, genstd_post, recor_sampled, recor_sampled_std)
    filename = "p3_same_axis_fluence_fitted.png"
    fig.savefig(plot_save_path/filename)

    recor_sampled = pd.DataFrame(data=genmeans_post, index=None, columns=['p0','p1','c','t'])
    recor_sampled_std = pd.DataFrame(data=genstd_post, index=None, columns=['p0','p1','c','t'])
    fig = pre_post_fluences(genmeans_pre, genstd_pre, recor_sampled, recor_sampled_std)
    filename = "p3_pre_post_fluences.png"
    fig.savefig(plot_save_path/filename)
