# %%
import faulthandler; faulthandler.enable()
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from common import make_data_dict, make_sur_map, make_bin_map, make_sur, myscore, param_list
from common import cut_range, clean_parameters, datafiles, devices, plot_all_hist_params
import collections
from pathlib import Path
sensor_data = make_data_dict()
# %%
pre_map =  make_sur_map(sensor_data['S8'][0])
post_map =  make_sur_map(sensor_data['S8'][1])
df2 = pd.read_csv("data/bin_map/EllipticIRRAD8.csv", skiprows=1, names=['col','row','bin'], sep=" ")
bin_map = make_bin_map(df2)

pre = pd.merge(sensor_data['S8'][0], df2, on=['col','row'])
post = pd.merge(sensor_data['S8'][1], df2, on=['col','row'])

def plot_sensor_bins():
    fig, axes = plt.subplots(1,1)
    im = axes.imshow(bin_map)
    fig.colorbar(im, ax=axes)
    axes.set_title('Sensor bins')
    return fig

def plot_different_bins():
    fig, axes = plt.subplots(2,4,figsize=(18,8))

    p0_range=(-4,8)
    p1_range=(0.002,0.007)
    c_range=(0,15000)
    t_range=(-2000,1500)
    axe = axes[0]

    ranges = p0_range, p1_range, c_range, t_range
    fig.suptitle("Surrogates histogram vs bins", fontsize=24)
    for name, group in pre.groupby('bin'):
        if str(name) in "1357":
            continue
        pre2 = cut_range(group)
        label = name
        plot_all_hist_params(pre2, axe, ranges, label=label, density=True)
    axe[3].legend(prop={'size': 10}, title='bins')
    axe[0].set_ylabel('pre')


    axe = axes[1]
    for name, group in post.groupby('bin'):
        if str(name) in "1357":
            continue
        #pre2= clean_parameters(group)
        pre2 = cut_range(group)
        plot_all_hist_params(pre2, axe, ranges, label=label, density=True)

    axe[0].set_xlabel('p0')
    axe[0].set_ylabel('post')
    axe[1].set_xlabel('p1')
    axe[2].set_xlabel('c')
    axe[3].set_xlabel('t')
    return fig


def plot_pre_post_bins():
    p0_range=(-10,10)
    p1_range=(0,0.010)
    c_range=(0,15000)
    t_range=(-2000,3000)
    param_list = ['p0','p1','c','t']
    param_ranges = dict(zip(param_list, [p0_range,p1_range, c_range, t_range]))
    # fig.suptitle("Pre and post irradiation by bin and parameter", fontsize=24)
    bin_to_fig = {}
    for (name1, pre2), (name2, post2) in zip(pre.groupby('bin'), post.groupby('bin')):
        assert name1 == name2
        fig, axes = plt.subplots(1,4,figsize=(15,4))
        for i, param in enumerate(param_list):
            ax = axes[i]
            pre2[param].hist(range=param_ranges[param], bins=100, ax=ax, alpha=0.5, label='pre', density=True)
            post2[param].hist(range=param_ranges[param], bins=100, ax=ax, alpha=0.5, label='post', density=True)
            axes[0].set_ylabel(f'bin {name1}')
            axes[i].set_xlabel(f'{param}')

            # axes[0][3].legend(prop={'size': 10})
        fig.tight_layout()
        bin_to_fig[name1] = (fig, axes)

    # for name, group in post.groupby('bin'):
    #     pre2 = group
    #     fig, axes = bin_to_fig[name]
    #     for i, param in enumerate(param_list):
    #         label = name
    #         ax = axes[i]
    #         pre2[param].hist(range=param_ranges[param], bins=100, ax=ax, alpha=0.5, label='post', density=True)
    return bin_to_fig

def plot_mean_bin_sur():
    fig, axes = plt.subplots(1,1,figsize=(10,4), sharey=True)
    max_tp_charge = 8000
    x = np.linspace(900, max_tp_charge, 1000)
    p0_range=(-10,10)
    p1_range=(0,0.010)
    c_range=(0,15000)
    t_range=(-2000,3000)
    axe = axes
    fig.suptitle("Surrogates based on mean parameters (not accounting correlation!) vs bins", fontsize=18)
    for name, group in pre.groupby('bin'):
        if str(name) in "1357":
            continue
        pre2 = cut_range(group)
        margs = pre2.mean()
        sur = make_sur(margs['p0'], margs['p1'], margs['c'], margs['t'])
        y = sur(x)
        axe.plot(x,y, '--',label='pre '+str(name))

    for name, group in post.groupby('bin'):
        if str(name) in "1357":
            continue
        pre2 = cut_range(group)
        margs = pre2.mean()
        sur = make_sur(margs['p0'], margs['p1'], margs['c'], margs['t'])
        y = sur(x)
        axe.plot(x,y,  label='post '+str(name))

    axe.legend(prop={'size': 10}, title='bins')
    axe.set_ylabel("TOT counts")
    axe.set_xlabel("Charge [$e^-$]")
    fig.tight_layout()
    return fig


def plot_boxes_bins():
    p0_range=(-4,8)
    p1_range=(0.002,0.007)
    c_range=(0,15000)
    t_range=(-2000,1500)

    pre_params = collections.defaultdict(list)
    post_params = collections.defaultdict(list)

    for name, group in pre.groupby('bin'):
        pre2 = cut_range(group)
        label = name
        for p_ in param_list:
            pre_params[p_].append(pre2[p_])
    for name, group in post.groupby('bin'):
        post2 = cut_range(group)
        label = name
        for p_ in param_list:
            post_params[p_].append(post2[p_])

    labels = list(map(str, range(8))  )
    fsize = (11,4)
    def _miniplot(param, param_pre, param_post):
        fig, ax = plt.subplots(1,2,figsize=fsize, sharey=True)
        fig.suptitle("S8 pre and post {} parameter dist. vs bins".format(param), fontsize=18)
        ax[0].boxplot(param_pre, labels=labels)
        ax[0].invert_xaxis()
        ax[0].set_ylabel(param, fontsize=16)
        ax[1].boxplot(param_post, labels=labels)
        ax[1].invert_xaxis()
        ax[0].set_xlabel('pre [bins]', fontsize=16)
        ax[1].set_xlabel('post [bins]', fontsize=16)
        fig.tight_layout()
        return fig

    figs= [_miniplot(p_, pre_params[p_], post_params[p_]) for p_ in param_list]
    return figs

if __name__ == '__main__':
    plot_save_path = Path('plots_presentation/')

    fig = plot_sensor_bins()
    filename = "p2_binning.png"
    fig.savefig(plot_save_path/filename)

    labels, counts = np.unique(bin_map, return_counts=True)
    area = counts * 55 * 55
    print(labels, counts, area)

    # fig = plot_different_bins()
    # filename = "p2_diff_bins.png"
    # fig.savefig(plot_save_path/filename)

    # figs_dict = plot_pre_post_bins()
    # for bin_name, (fig, axes) in figs_dict.items():
    #     filename = "p2_pre_post_bin_{}.png".format(bin_name)
    #     fig.savefig(plot_save_path/filename)

    fig = plot_mean_bin_sur()
    filename = "p2_bins_surrogates.png"
    fig.savefig(plot_save_path/filename)

    figs = plot_boxes_bins()
    for i, f in enumerate(figs):
        filename = "p2_box_plot{}.png".format(i)
        f.savefig(plot_save_path/filename)
