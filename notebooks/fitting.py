# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.10.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from common import make_data_dict

sensor_data = make_data_dict()

# %%
def make_params_plot(df, sensor, typ, destination, rang=None):
    fig,axes = plt.subplots(2,2, figsize=(8,8))
    fig.suptitle(sensor+ " " +typ, fontsize=16)
    ax = df['p1'].hist(ax=axes[0][0],bins=100)

    ax.set_title("p1 distribution all pixels bins=100")

    ax = df['p0'].hist(ax=axes[0][1],bins=100)

    ax.set_title("p0 distribution all pixels bins=100")

    ax = df['c'].hist(ax=axes[1][0],bins=100)

    ax.set_title("c distribution all pixels bins=100")

    rang = df['t'].min(), df['t'].max()
    ax = df['t'].hist(ax=axes[1][1],bins=100, range=rang)
    ax.set_title("t distribution all pixels bins=100")

    fig.tight_layout()
    fig.savefig(destination/"{}_{}_param_hist.png".format(sensor, typ.lower()))



# %%

from matplotlib import pyplot as plt
import numpy as np
from sklearn.mixture import GaussianMixture


def make_3_plot(data, sensor, typ,destination, rang=None, ga_kwrgs={}):
    dataclean = data['t'][~data['t'].isna()]
    X = dataclean.values.reshape(-1,1)
    if rang is None:
        rang = (X.min(), X.max())

    model = GaussianMixture(3, **ga_kwrgs).fit(X)
    x = np.linspace(rang[0], rang[1], 1000)
    logprob = model.score_samples(x.reshape(-1, 1))
    responsibilities = model.predict_proba(x.reshape(-1, 1))
    pdf = np.exp(logprob)
    pdf_individual = responsibilities * pdf[:, np.newaxis]
    
    fig = plt.figure(figsize=(30, 6))
    ax = fig.add_subplot(131)
    ax.hist(X, 100, density=True, histtype='stepfilled', alpha=0.4, range=rang)
    ax.plot(x, pdf, '-k')
    ax.plot(x, pdf_individual, '--k')
    ax.text(0.04, 0.96, "Best-fit Mixture",
            ha='left', va='top', transform=ax.transAxes)
    ax.set_xlabel('$x$')
    ax.set_ylabel('$p(x)$')
    ax.set_title("{}_{} fit".format(sensor, typ.lower()))
    fig.savefig(destination/"{}_{}_fit.png".format(sensor, typ.lower()))
    return model



# %%
#['S6', 'S8', 'S16', 'S17', 'S21', 'S22', 'S27', 'S29', 'S30']

plotpath = Path('plots')


sensor = 'S6'
before, post = sensor_data[sensor]
rang = (-1000,2000)  
make_params_plot(before, sensor, 'before', plotpath, rang=rang)
rang = (-1000,2000)   
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
rang = (-2000,3000)  
gm = make_3_plot(before, sensor, 'before', plotpath, rang=rang)
ga_kwrgs = dict(means_init=gm.means_, precisions_init=gm.precisions_, weights_init=gm.weights_)
make_3_plot(post, sensor, 'post', plotpath, rang=rang, ga_kwrgs=ga_kwrgs)
    
    
sensor = 'S8'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
rang = (-2000,3000)       
make_params_plot(post, sensor, 'post', plotpath,rang=rang)
make_3_plot(before, sensor, 'before', plotpath)
rang = (-2000,2000)       
make_3_plot(post, sensor, 'post', plotpath,rang=rang)
    
    
sensor = 'S16'
before, post = sensor_data[sensor]
rang = (-1000,2000)
make_params_plot(before, sensor, 'before', plotpath, rang=rang)
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
rang = (-2000,2000)
make_3_plot(before, sensor, 'before', plotpath, rang=rang)
rang = (-700,2000)
means = np.array([[560], [560], [560]])
weights = np.array([0.7, 0.2, 0.1])
gw = {"weights_init": weights, "means_init":means}
make_3_plot(post, sensor, 'post', plotpath, rang=rang, ga_kwrgs=gw)


sensor = 'S17'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
make_params_plot(post, sensor, 'post', plotpath)
make_3_plot(before, sensor, 'before', plotpath)
make_3_plot(post, sensor, 'post', plotpath)
    
sensor = 'S21'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
rang = (-1000,2000)
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
make_3_plot(before, sensor, 'before', plotpath)
rang = (-1000,2000)
make_3_plot(post, sensor, 'post', plotpath, rang=rang)
    

sensor = 'S22'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
make_params_plot(post, sensor, 'post', plotpath)
make_3_plot(before, sensor, 'before', plotpath)
make_3_plot(post, sensor, 'post', plotpath)
    
    
sensor = 'S27'
before, post = sensor_data[sensor]
rang = (-1000,2000)
make_params_plot(before, sensor, 'before', plotpath, rang=rang)
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
rang = (-1000,2000)
make_3_plot(before, sensor, 'before', plotpath, rang=rang)
make_3_plot(post, sensor, 'post', plotpath, rang=rang)


sensor = 'S29'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
rang = (-2000,3000)        
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
make_3_plot(before, sensor, 'before', plotpath)
rang = (-2000,3000)        
make_3_plot(post, sensor, 'post', plotpath, rang=rang)


sensor = 'S30'
before, post = sensor_data[sensor]
make_params_plot(before, sensor, 'before', plotpath)
rang = (-1000,2000)
make_params_plot(post, sensor, 'post', plotpath, rang=rang)
make_3_plot(before, sensor, 'before', plotpath)
rang = (-1000,2000)
make_3_plot(post, sensor, 'post', plotpath, rang=rang)


# %%
weights.shape

# %%
vals, edges, _ = plt.hist(b['t'], bins=100)

# %%
edges[:-1]

# %%
from scipy.optimize import curve_fit
from scipy.stats import crystalball
b, p = sensor_data['S22']


def func(x, *params):
    y = np.zeros_like(x)
    for i in range(0, len(params), 4):
        beta = params[i]
        m = params[i+1]
        loc = params[i+2]
        scale = params[i+3]
        y += crystalball.pdf(x, beta, m, loc, scale)
    return y

popt, pcov = curve_fit(func, edges[:-1], vals, p0 = [1.5,2.1,600,40])
fit = func(edges[:-1], *popt)
plt.plot(edges[:-1],vals)
plt.plot(edges[:-1], fit , 'r-')
plt.show()


# %%
from scipy.stats import norm
a, b = 1., 2.
x = norm.rvs(a, b, size=1000, random_state=123)
loc1, scale1 = norm.fit(x)
loc1, scale1


# %%
plt.plot(x)

# %%
