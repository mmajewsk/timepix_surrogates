# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.10.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from common import make_data_dict, make_sur_map, make_bin_map, make_sur, myscore, cut_range, clean_parameters
from collections import OrderedDict

sensor_data = make_data_dict()
# %%
pre_map =  make_sur_map(sensor_data['S8'][0])
post_map =  make_sur_map(sensor_data['S8'][1])
BINNING_TYPE = '8'
df2 = pd.read_csv("data/bin_map/EllipticIRRAD{}.csv".format(BINNING_TYPE), skiprows=1, names=['col','row','bin'], sep=" ")
bin_map = make_bin_map(df2)
binsize = len(df2['bin'].unique())
pre = pd.merge(sensor_data['S8'][0], df2, on=['col','row'])
post = pd.merge(sensor_data['S8'][1], df2, on=['col','row'])

# %%
im = plt.imshow(bin_map)
plt.colorbar(im)
plt.title('Sensor bins')
# %%
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.stats import crystalball, norm, skewnorm


class NoPcaSuroGen:
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self):
        self.dist = crystalball
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()
        self.scaler = StandardScaler()
        
        
    def transform(self, data):
        data = self.scaler.transform(data)
        return data
        
    def inverse_transform(self, data):
        data = self.scaler.inverse_transform(data)
        return data
        
    @staticmethod
    def prepare_data(data):
        pre_params = data[NoPcaSuroGen._columns]
        pre_cut_bin = cut_range(pre_params)
        return pre_cut_bin

    def _generate(self, size):
        arr = np.zeros((size, len(self._columns)))
        for j, col in enumerate(self._columns):
            arr[:,j] = self.fitted_gens[col](size)
        return arr
        
    def generate(self, size):
        arr = self._generate(size)
        generated = self.inverse_transform(arr)
        return generated
        
    def _fit(self, data):
        for j, col in enumerate(self._columns):
            d = data[:,j]
            d = d[~np.isnan(d)]
            p = self.dist.fit(d)
            self.fit_params[col] = p
            self.fitted_gens[col] = lambda x=1 : self.dist.rvs(*p,x)
        
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        self._fit(data)
            
    def means(self):
        mean_arr = [self.dist.mean(*params) for params in self.fit_params.values()]
        mean_tran= self.inverse_transform([mean_arr])
        return mean_tran
    
class SuroGen(NoPcaSuroGen):
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self):
        #self.dist = skewnorm
        NoPcaSuroGen.__init__(self)
        self.dist = skewnorm
        self.scaler = StandardScaler()
        self.pca = PCA(whiten=True)
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()

    def transform(self, data):
        data = self.scaler.transform(data)
        data = self.pca.transform(data)
        return data

    def inverse_transform(self, data):
        data = self.pca.inverse_transform(data)
        data = self.scaler.inverse_transform(data)
        return data
    
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        decor = self.pca.fit_transform(data)
        self._fit(decor)
    
    def generate_decor(self, size):
        return NoPcaSuroGen._generate(self, size)

    def generate(self, size):
        arr = self.generate_decor(size)
        generated = self.inverse_transform(arr)
        return generated
    
class SuroGen2(SuroGen):
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self,pca):
        SuroGen.__init__(self)
        self.dist = skewnorm
        self.pca = pca
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()

    
    def fit_alteration(total_data):
        pre_cut_bin = SuroGen2.prepare_data(total_data)
        scaler = StandardScaler()
        pca = PCA(whiten=True)
        data = scaler.fit_transform(pre_cut_bin)
        decor = pca.fit_transform(data)
        return scaler, pca
    
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        decor = self.pca.transform(data)
        self._fit(decor)
    

# %%
fig, ax = plt.subplots(4,4, figsize=(24,15))

axes = ax[0]
pre_cut = SuroGen.prepare_data(pre)
for m in range(4):
    axes[m].hist(pre_cut.values[:,m],bins=100)
    

GENSIZE = pre.shape[0]
axes = ax[3]
tmpbins = []
for m in range(4):
    _, bins, _  = axes[m].hist(pre_cut.values[:,m],bins=50, label="raw data", alpha=0.5)
    tmpbins.append(bins)
    
axes = ax[2]
sgn = SuroGen()
sgn.fit(pre)
decorelated = sgn.generate_decor(GENSIZE)
_ = axes[0].hist(decorelated[:,0], bins=100, label="original (n={})".format(GENSIZE))
_ = axes[1].hist(decorelated[:,1], bins=100, label="original (n={})".format(GENSIZE))
_ = axes[2].hist(decorelated[:,2], bins=100, label="original (n={})".format(GENSIZE))
_ = axes[3].hist(decorelated[:,3], bins=100, label="original (n={})".format(GENSIZE))

generated = np.zeros_like(decorelated)
axes = ax[1]
_ = axes[0].hist(decorelated[:,0], bins=100, density=True)
_ = axes[1].hist(decorelated[:,1], bins=100, density=True)
_ = axes[2].hist(decorelated[:,2], bins=100, density=True)
_ = axes[3].hist(decorelated[:,3], bins=100, density=True)


EXAMPLE_GENSIZE = 35000
exmpl = sgn.generate_decor(EXAMPLE_GENSIZE)
for j in range(4):
    data = decorelated[:,j]
    params = list(sgn.fit_params.items())[j][1]
    size = 1000 
    x = np.linspace(data.min(), data.max(), size)
    pdf_fitted = sgn.dist.pdf(x, *params) 
    axes = ax[1]
    axes[j].plot(x, pdf_fitted)
    # = dist.rvs(GENSIZE)
    example_gen = exmpl[:,j]
    axes = ax[2]
    axes[j].hist(example_gen, bins=100, label="generated samples (n={})".format(EXAMPLE_GENSIZE))
    axes[j].legend()
    
    
recor = sgn.generate(GENSIZE)
    
axes = ax[3]
for m in range(4):
    axes[m].hist(recor[:,m],bins=tmpbins[m], label="generated", alpha=0.5)
    axes[m].legend()

ax[0,0].set_ylabel("raw data")
ax[1,0].set_ylabel("rescaled and decorelated")
ax[2,0].set_ylabel("rescaled, decoralted and fitted")
ax[3,0].set_ylabel("raw data and generated")
ax[3,0].set_xlabel("p0")
ax[3,1].set_xlabel("p1")
ax[3,2].set_xlabel("c")
ax[3,3].set_xlabel("t")
fig.show()
import seaborn as sns
plt.figure()
corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
sns.heatmap(corrMatrix, annot=True)
plt.show()

# %%
fig, ax = plt.subplots(binsize,4, figsize=(12,16))
dist = skewnorm
for i, (name, group) in enumerate(pre.groupby('bin')):
    axes = ax[i]
    pre_params = group[['p0','p1','c','t']]
    pre_cut_bin = cut_range(pre_params)
    surogen = SuroGen()
    surogen.fit(group)
    if i == 9:
        generated = group.values
    else:
        generated = surogen.generate(group.shape[0])

    _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
    _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
    _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
    _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

    axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
    axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
    axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
    axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
    axes[0].set_ylabel("Bin {}".format(name))

axes[0].set_xlabel('p0')
axes[1].set_xlabel('p1')
axes[2].set_xlabel('c')
axes[3].set_xlabel('t')
ax[0,3].legend()










# %%
    
fig, ax = plt.subplots(binsize,5, figsize=(18,18))
generated_all = []

genmeans = []
gmeans_dict = {}
surogen_in_bin = {}
corr_in_bin = {}

#generated_to_save_all = {}
#params_in_bin = {}
post_surogen = {}
fig.suptitle("S8 post generated vs and original distribution per bin", fontsize=24)
for i, (name, group) in enumerate(post.groupby('bin')):
    if name==9:
        break
    axes = ax[i]
    pre_params = group[['p0','p1','c','t']]
    pre_cut_bin = cut_range(pre_params)
    surogen = SuroGen()
    surogen.fit(group)
    corr_in_bin[name] = surogen.pca.components_
    generated = surogen.generate(group.shape[0])
    surogen_in_bin[name] = surogen
    generated_all.append(generated)
    rmi = surogen.means()[0]
    genmeans.append(rmi)
    gmeans_dict[i] = rmi
    #print([v for v in surogen.fit_params.values()])
    #print(rmi)
    axes[0].axvline(rmi[0])
    axes[1].axvline(rmi[1])
    axes[2].axvline(rmi[2])
    axes[3].axvline(rmi[3])
    
    _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
    _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
    _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
    _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

    axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
    axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
    axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
    axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
    axes[0].set_ylabel("Bin {}".format(name))
    decorelated = surogen.transform(SuroGen.prepare_data(group))
    corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
    sns.heatmap(corrMatrix, annot=True, ax=axes[4])

ax[0,3].legend()
axes[0].set_xlabel('p0')
axes[1].set_xlabel('p1')
axes[2].set_xlabel('c')
axes[3].set_xlabel('t')



# %%
    
fig, ax = plt.subplots(binsize,5, figsize=(18,18))
generated_all = []

genmeans = []
gmeans_dict = {}
surogen_in_bin = {}
corr_in_bin = {}
scaler, pca = SuroGen2.fit_alteration(post)
#generated_to_save_all = {}
#params_in_bin = {}
post_surogen = {}
fig.suptitle("S8 post generated vs and original distribution per bin", fontsize=24)
for i, (name, group) in enumerate(post.groupby('bin')):
    if name==9:
        break
    axes = ax[i]
    pre_params = group[['p0','p1','c','t']]
    pre_cut_bin = cut_range(pre_params)
    surogen = SuroGen2(pca)
    surogen.fit(group)
    corr_in_bin[name] = pre_cut_bin.corr()
    generated = surogen.generate(group.shape[0])
    surogen_in_bin[name] = surogen
    generated_all.append(generated)
    rmi = surogen.means()[0]
    genmeans.append(rmi)
    gmeans_dict[i] = rmi
    #print([v for v in surogen.fit_params.values()])
    #print(rmi)
    axes[0].axvline(rmi[0])
    axes[1].axvline(rmi[1])
    axes[2].axvline(rmi[2])
    axes[3].axvline(rmi[3])
    
    _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
    _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
    _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
    _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

    axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
    axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
    axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
    axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
    axes[0].set_ylabel("Bin {}".format(name))
    decorelated = surogen.transform(SuroGen.prepare_data(group))
    corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
    sns.heatmap(corrMatrix, annot=True, ax=axes[4])

ax[0,3].legend()
axes[0].set_xlabel('p0')
axes[1].set_xlabel('p1')
axes[2].set_xlabel('c')
axes[3].set_xlabel('t')



# %%
all_gen_arr = np.concatenate(generated_all)
post_cut = post[['p0','p1','c','t']]
post_cut = cut_range(post_cut)

fig, ax = plt.subplots(1,4, figsize=(16,4))
fig.suptitle("S8 post generated vs and original distribution on whole sensor", fontsize=24)
axes = ax
for j in range(4):
    _, b, _ = axes[j].hist(post_cut.values[:,j], bins=100, alpha=0.6, label='raw')
    axes[j].hist(all_gen_arr[:,j], bins=b, alpha=0.6, label='generated')
axes[0].set_xlabel("p0")
axes[1].set_xlabel("p1")
axes[2].set_xlabel("c")
axes[3].set_xlabel("t")
axes[3].legend()

# %%
#plt.style.use("bmh")
fig, axes = plt.subplots(1,1,figsize=(18,8), sharey=True)
max_tp_charge = 8000
x = np.linspace(900, max_tp_charge, 100)
p0_range=(-10,10)
p1_range=(0,0.010)
c_range=(0,15000)
t_range=(-2000,3000)
axe = axes
fig.suptitle("Original (n=300) and generated (n=300) surrogates S8 pre - generated for whole sensor", fontsize=16)
sampled_pre = pre_cut.sample(300)
for i,row in sampled_pre.iterrows():
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, color='blue', alpha=0.2)

axe.plot(xfilt,y, color='blue', alpha=0.2, label='original')
recor_sampled = pd.DataFrame(data=recor, index=None, columns=['p0','p1','c','t']).sample(300) 
for i,row in recor_sampled.iterrows():
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, color='orange', alpha=0.2)
    
axe.plot(xfilt,y, color='orange', alpha=0.2, label='generated')
axe.legend(prop={'size': 10})


# %%
#plt.style.use("bmh")
fig, axes = plt.subplots(1,1,figsize=(18,8), sharey=True)
max_tp_charge = 8000
x = np.linspace(900, max_tp_charge, 100)
p0_range=(-10,10)
p1_range=(0,0.010)
c_range=(0,15000)
t_range=(-2000,3000)
axe = axes
fig.suptitle("Original (n=300) and generated (n=300) surrogates, S8 post - generated per bin", fontsize=16)
sampled_pre = post_cut.sample(300)
for i,row in sampled_pre.iterrows():
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, color='blue', alpha=0.2)

axe.plot(xfilt,y, color='blue', alpha=0.2, label='original')
recor_sampled = pd.DataFrame(data=all_gen_arr, index=None, columns=['p0','p1','c','t']).sample(300) 
for i,row in recor_sampled.iterrows():
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, color='orange', alpha=0.2)
    
axe.plot(xfilt,y, color='orange', alpha=0.2, label='generated')
axe.legend(prop={'size': 10})


# %%

# %%

genmeans
# %%
#plt.style.use("bmh")
fig, axes = plt.subplots(1,1,figsize=(18,8), sharey=True)
max_tp_charge = 8000
x = np.linspace(900, max_tp_charge, 100)
p0_range=(-10,10)
p1_range=(0,0.010)
c_range=(0,15000)
t_range=(-2000,3000)
axe = axes
fig.suptitle("Original (n=300) and generated (n=7) surrogates, S8 post - generated per bin", fontsize=16)
sampled_pre = post_cut.sample(300)
for i,row in sampled_pre.iterrows():
    break
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, color='blue', alpha=0.2)

axe.plot(xfilt,y, color='blue', alpha=0.2, label='original')
recor_sampled = pd.DataFrame(data=genmeans, index=None, columns=['p0','p1','c','t'])
for i,row in recor_sampled.iterrows():
    sur = make_sur(row['p0'], row['p1'], row['c'], row['t'])
    xfilt = x[x>row['t']]
    y = sur(xfilt)
    xfilt = xfilt[y>-50]
    y = y[y>-50]
    axe.plot(xfilt,y, label=str(i))
    
axe.plot(xfilt,y, color='orange', alpha=0.2, label='generated')
axe.legend(prop={'size': 10})


# %%
generated = np.stack(df2['bin'].map(gmeans_dict).values)
gen_df = df2.copy()
gen_df['p0'] = generated[:,0]
gen_df['p1'] = generated[:,1]
gen_df['c'] = generated[:,2]
gen_df['t'] = generated[:,3]

# %%
heder = "Charge W0009_G08  \n## fit function ToT = f(q) = [p0 + p1*q - c/(q-t)]\n## col  row   p0   p1   c   t\n"
#del gen_dist['bin']
fname = 'Bin{}_s8_post_generated_by_means.csv'.format(BINNING_TYPE)
with open(fname, 'w') as f:
    f.write(heder)
    del gen_df['bin']
    gen_df.to_csv(f, index=False, sep=' ', header=None, mode='a')

# %%
gen_dist = df2.copy()
gen_dist['p0'] = None
gen_dist['p1'] = None
gen_dist['c'] = None
gen_dist['t'] = None
for k,v in enumerate(generated_all):
    gen_dist.loc[gen_dist['bin']==k,['p0','p1','c','t']] = v 

# %%
fig, axes = plt.subplots(1,4, figsize=(16,4))
for k, ax in zip(['p0','p1','c','t'], axes):
    _,bins,_ =  ax.hist(gen_dist[k], bins=100, alpha=0.7)
    post_cut[k].hist(bins=bins, alpha=0.7, ax=ax)

# %%
heder = "Charge W0009_G08  \n## fit function ToT = f(q) = [p0 + p1*q - c/(q-t)]\n## col  row   p0   p1   c   t\n"
#del gen_dist['bin']
fname = 'Bin{}_s8_post_generated_by_dist.csv'.format(BINNING_TYPE)
with open(fname, 'w') as f:
    f.write(heder)
    del gen_dist['bin']
    gen_dist.to_csv(f,mode='a', index=False, sep=' ', header=None)

# %% [markdown]
# # Fitting to fluence

# %%
corvals = np.zeros((8,6))
for k,v in surogen_in_bin.items():
    m = corr_in_bin[k].values
    corvals[int(k),0] = m[1,0]
    corvals[int(k),1] = m[2,0]
    corvals[int(k),2] = m[2,1]
    corvals[int(k),3] = m[3,0]
    corvals[int(k),4] = m[3,1]
    corvals[int(k),5] = m[3,2]
    #print(v.pca.explained_variance_)

# %%
for i in range(6):
    plt.plot(corvals[:7,i])

# %%
corvals = np.zeros((8,6))
for k,v in surogen_in_bin.items():
    m = np.abs(corr_in_bin[k])
    #m = corr_in_bin[k]
    corvals[int(k),0] = m[1,0]
    corvals[int(k),1] = m[2,0]
    corvals[int(k),2] = m[2,1]
    corvals[int(k),3] = m[3,0]
    corvals[int(k),4] = m[3,1]
    corvals[int(k),5] = m[3,2]
    #print(v.pca.explained_variance_)

# %%
sur

# %%
for i in range(6):
    plt.plot(corvals[:7,i])

# %%
corvals[0,:]

# %% [markdown]
# # Fit no PCA

# %%

# %%
fig, ax = plt.subplots(4,4, figsize=(24,15))

axes = ax[0]
pre_cut = NoPcaSuroGen.prepare_data(pre)
for m in range(4):
    axes[m].hist(pre_cut.values[:,m],bins=100)
    

GENSIZE = pre.shape[0]
axes = ax[3]
tmpbins = []
for m in range(4):
    _, bins, _  = axes[m].hist(pre_cut.values[:,m],bins=50, label="raw data", alpha=0.5)
    tmpbins.append(bins)
    
axes = ax[2]
sgn = NoPcaSuroGen()
sgn.fit(pre)
decorelated = sgn.generate(GENSIZE)

generated = np.zeros_like(decorelated)
axes = ax[1]
unc = sgn._generate(GENSIZE)
_ = axes[0].hist(unc[:,0], bins=100, density=True)
_ = axes[1].hist(unc[:,1], bins=100, density=True)
_ = axes[2].hist(unc[:,2], bins=100, density=True)
_ = axes[3].hist(unc[:,3], bins=100, density=True)


EXAMPLE_GENSIZE = 35000
exmpl = sgn.generate(EXAMPLE_GENSIZE)
for j in range(4):
    data = unc[:,j]
    params = list(sgn.fit_params.items())[j][1]
    print(params)
    print(sgn.fit_params)
    size = 1000 
    x = np.linspace(data.min(), data.max(), size)
    pdf_fitted = sgn.dist.pdf(x, *params) 
    axes = ax[1]
    axes[j].plot(x, pdf_fitted)
    # = dist.rvs(GENSIZE)
    example_gen = exmpl[:,j]
    axes = ax[2]
    _, b,_ = axes[j].hist(example_gen, bins=100, label="generated samples (n={})".format(EXAMPLE_GENSIZE), alpha=0.5)
    axes[j].hist(decorelated[:,j], bins=b, label="original (n={})".format(GENSIZE), alpha=0.5)
    axes[j].legend()
    
    
recor = sgn.generate(GENSIZE)
    
axes = ax[3]
for m in range(4):
    axes[m].hist(recor[:,m],bins=tmpbins[m], label="generated", alpha=0.5)
    axes[m].legend()

ax[0,0].set_ylabel("raw data")
ax[1,0].set_ylabel("rescaled and decorelated")
ax[2,0].set_ylabel("rescaled, decoralted and fitted")
ax[3,0].set_ylabel("raw data and generated")
ax[3,0].set_xlabel("p0")
ax[3,1].set_xlabel("p1")
ax[3,2].set_xlabel("c")
ax[3,3].set_xlabel("t")
fig.show()
import seaborn as sns
plt.figure()
corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
sns.heatmap(corrMatrix, annot=True)
plt.show()

# %%
    
fig, ax = plt.subplots(binsize,5, figsize=(18,18))
generated_all = []

genmeans = []
gmeans_dict = {}

#generated_to_save_all = {}
#params_in_bin = {}
post_surogen = {}
fig.suptitle("S8 post generated vs and original distribution per bin", fontsize=24)
for i, (name, group) in enumerate(post.groupby('bin')):
    if name==9:
        break
    axes = ax[i]
    pre_params = group[['p0','p1','c','t']]
    pre_cut_bin = cut_range(pre_params)
    surogen = NoPcaSuroGen()
    surogen.fit(group)
    generated = surogen.generate(group.shape[0])
    #surogen_in_bin[name] = surogen
    generated_all.append(generated)
    rmi = surogen.means()[0]
    genmeans.append(rmi)
    gmeans_dict[i] = rmi
    print([v for v in surogen.fit_params.values()])
    print(rmi)
    axes[0].axvline(rmi[0])
    axes[1].axvline(rmi[1])
    axes[2].axvline(rmi[2])
    axes[3].axvline(rmi[3])
    
    _, b1, _ = axes[0].hist(pre_cut_bin.values[:,0], bins=100, alpha=0.4)
    _, b2, _ = axes[1].hist(pre_cut_bin.values[:,1], bins=100, alpha=0.4)
    _, b3, _ = axes[2].hist(pre_cut_bin.values[:,2], bins=100, alpha=0.4)
    _, b4, _ = axes[3].hist(pre_cut_bin.values[:,3], bins=100, alpha=0.4, label='raw')

    axes[0].hist(generated[:,0], bins=b1, alpha=0.4)
    axes[1].hist(generated[:,1], bins=b2, alpha=0.4)
    axes[2].hist(generated[:,2], bins=b3, alpha=0.4)
    axes[3].hist(generated[:,3], bins=b4, alpha=0.4, label='generated')
    axes[0].set_ylabel("Bin {}".format(name))
    decorelated = surogen.transform(NoPcaSuroGen.prepare_data(group))
    corrMatrix = pd.DataFrame(data=decorelated, columns=SuroGen._columns).corr()
    sns.heatmap(corrMatrix, annot=True, ax=axes[4])

ax[0,3].legend()
axes[0].set_xlabel('p0')
axes[1].set_xlabel('p1')
axes[2].set_xlabel('c')
axes[3].set_xlabel('t')



# %%

# %%
