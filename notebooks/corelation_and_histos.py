# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.10.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from common import make_data_dict, make_sur_map, make_bin_map, make_sur, myscore, cut_range, clean_parameters

# %%
data_folder = Path("../data/sur/")

# %%
datafiles = [
    
    
#"S4_BeforeKIT_Coarse64_17032015_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#"S4_PostKIT_1000V_07122015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S6_BeforeJSI_Coarse64_13032015_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S6_PostJSI_600V_TestPulse_iKrum20_postIrrad_16oct2015_surrog_fitpars_perpix_NNsmoothingON.dat
"S6_PostJSI_600V_TestPulse_iKrum25_postIrrad_16oct2015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S8_BeforeIRRAD_200V_TestPulse_SpidrTime_18May_surrog_fitpars_perpix_NNsmoothingON.dat",
#S8_PostIRRAD_800V_postIrradEqualise_IKRUM20_29092015_surrog_fitpars_perpix_NNsmoothingON.dat
"S8_postIrrad_800V_preIrradEqualise_iKrum20_THL426_29092015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S16_BeforeKIT_Coarse64_05112014_surrog_fitpars_perpix_NNsmoothingON.dat",
"S16_PostKIT_25072015_400V_surrog_fitpars_perpix_NNsmoothingON.dat",

"S17_BeforeJSI_Coarse64_18032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S17_PostJSI_1000V_TestPulse_24May_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat
#S17_PostJSI_400V_TestPulse_24May_Ikrum50_surrog_fitpars_perpix_NNsmoothingON.dat
"S17_PostJSI_IKRUM10_400V_TestPulse_24May_surrog_fitpars_perpix_NNsmoothingON.dat",

"S21_BeforeIRRAD_Coarse64_04122014_surrog_fitpars_perpix_NNsmoothingON.dat",
"S21_PostIRRAD_200V_TestPulse_SpidrTime_240315_surrog_fitpars_perpix_NNsmoothingON.dat",

"S22_BeforeJSI_200V_TestPulse_SpidrTime_170315_surrog_fitpars_perpix_NNsmoothingON.dat",
#S22_PostJSI_400V_Testpulse_24May_surrog_fitpars_perpix_NNsmoothingON.dat
"S22_PostJSI_450V_ikrum20_TestPulse_SpidrTime_30112016_surrog_fitpars_perpix_NNsmoothingON.dat",

"S27_BeforeJSI_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
"S27_PostJSI_1000V_TestPulse_25May_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat",

"S29_BeforeJSI_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S29_PostJSI_1000V_TestPulse_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat
"S29_PostJSI_1000V_TestPulse_Ikrum50_surrog_fitpars_perpix_NNsmoothingON.dat",

"S30_BeforeIRRAD_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S30_PostIRRAD_600V_TestPulse_25jan2016_surrog_fitpars_perpix_NNsmoothingON.dat,
"S30_postIrrad_800V_27092015_surrog_fitpars_perpix_NNsmoothingON.dat",
]
devices = [6, 8, 16, 17, 21, 22, 27, 29, 30]

# %%
paired_data = list(zip(datafiles[::2], datafiles[1::2]))

# %%
sensor_data = {}
for before, post in paired_data:
    sensor = before.split('_')[0]
    df_b = pd.read_csv(data_folder/before, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
    df_p = pd.read_csv(data_folder/post, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
    sensor_data[sensor] = (df_b, df_p)


# %%
tested = sensor_data['S16'][1][['t']]
fig, ax = plt.subplots(1,1)
mean = tested.mean()['t']
std = tested.std()['t']
print(mean, std)
tested = tested[myscore(tested) < 0.01]
tested.hist(bins=100, log=True, ax=ax)
ax.axvline(mean, color='red')
#ax.axvline(mean-std, color='green')

# %%
from scipy import stats

p0_range=(-10,10)
p1_range=(0,0.010)
c_range=(0,15000)
t_range=(-2000,3000)

fig, axes = plt.subplots(len(sensor_data),4,figsize=(18,40))
for i, (sensor_name, (pre, post)) in enumerate(sensor_data.items()):
    axe= axes[i]
    
    pre2 = clean_parameters(pre)[['p0','p1','c','t']]
    pre2['p0'].hist(range=p0_range, bins=100, ax=axe[0], alpha=0.5)
    pre2['p1'].hist(range=p1_range,bins=100, ax=axe[1], alpha=0.5)
    pre2['c'].hist(range=c_range, bins=100, ax=axe[2], alpha=0.5)
    pre2['t'].hist(range=t_range,bins=100, ax=axe[3], alpha=0.5)
    
    
    
    post2= clean_parameters(post)[['p0','p1','c','t']]
    axe[0].set_ylabel(sensor_name)
    post2['p0'].hist(range=p0_range,bins=100, ax=axe[0], alpha=0.5)
    post2['p1'].hist(range=p1_range, bins=100, ax=axe[1], alpha=0.5)
    post2['c'].hist(range=c_range, bins=100, ax=axe[2], alpha=0.5)
    post2['t'].hist(range=t_range,bins=100, ax=axe[3], alpha=0.5)
    for j, param in zip(range(4), ['p0','p1','c','t']):
        axe[j].set_xlabel(param)
fig.tight_layout()
fig.show()

# %%
from scipy.stats import pearsonr
import seaborn as sns
import matplotlib.pyplot as plt 

def corrfunc(x,y, ax=None, **kws):
    """Plot the correlation coefficient in the top left hand corner of a plot."""
    r, pval = pearsonr(x, y)
    ax = ax or plt.gca()
    # Unicode for lowercase rho (ρ)
    rho = '\u03C1'
    ax.annotate(f'{rho} = {r:.2f} pval = {pval}', xy=(.1, .9), xycoords=ax.transAxes)


def plotboth(data, title):
    data = data.dropna()
    pre = data
    pre2 = clean_parameters(pre)[['p0','p1','c','t']]
    d = pre2[['p0','p1','c','t']]
    corrMatrix = d.corr()
    sns.heatmap(corrMatrix, annot=True).set_title(title)
    plt.show()
    g = sns.pairplot(d)
    g.fig.suptitle(title, y=1.08) 
    g.map_lower(corrfunc)
    plt.show()

for sensor_name, (pre, post) in sensor_data.items():
    title = "{} {}".format(sensor_name, 'pre')
    plotboth(pre, title)
    title = "{} {}".format(sensor_name, 'post')
    plotboth(post, title)
    
    

# %%

# %%
