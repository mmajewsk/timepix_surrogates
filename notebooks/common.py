# %%

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

data_folder = Path("./data/sur/")

datafiles = [

#"S4_BeforeKIT_Coarse64_17032015_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#"S4_PostKIT_1000V_07122015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S6_BeforeJSI_Coarse64_13032015_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S6_PostJSI_600V_TestPulse_iKrum20_postIrrad_16oct2015_surrog_fitpars_perpix_NNsmoothingON.dat
"S6_PostJSI_600V_TestPulse_iKrum25_postIrrad_16oct2015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S8_BeforeIRRAD_200V_TestPulse_SpidrTime_18May_surrog_fitpars_perpix_NNsmoothingON.dat",
"S8_PostIRRAD_800V_postIrradEqualise_IKRUM20_29092015_surrog_fitpars_perpix_NNsmoothingON.dat",
# "S8_postIrrad_800V_preIrradEqualise_iKrum20_THL426_29092015_surrog_fitpars_perpix_NNsmoothingON.dat",

"S16_BeforeKIT_Coarse64_05112014_surrog_fitpars_perpix_NNsmoothingON.dat",
"S16_PostKIT_25072015_400V_surrog_fitpars_perpix_NNsmoothingON.dat",

"S17_BeforeJSI_Coarse64_18032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S17_PostJSI_1000V_TestPulse_24May_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat
#S17_PostJSI_400V_TestPulse_24May_Ikrum50_surrog_fitpars_perpix_NNsmoothingON.dat
"S17_PostJSI_IKRUM10_400V_TestPulse_24May_surrog_fitpars_perpix_NNsmoothingON.dat",

"S21_BeforeIRRAD_Coarse64_04122014_surrog_fitpars_perpix_NNsmoothingON.dat",
"S21_PostIRRAD_200V_TestPulse_SpidrTime_240315_surrog_fitpars_perpix_NNsmoothingON.dat",

"S22_BeforeJSI_200V_TestPulse_SpidrTime_170315_surrog_fitpars_perpix_NNsmoothingON.dat",
#S22_PostJSI_400V_Testpulse_24May_surrog_fitpars_perpix_NNsmoothingON.dat
"S22_PostJSI_450V_ikrum20_TestPulse_SpidrTime_30112016_surrog_fitpars_perpix_NNsmoothingON.dat",

"S27_BeforeJSI_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
"S27_PostJSI_1000V_TestPulse_25May_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat",

"S29_BeforeJSI_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S29_PostJSI_1000V_TestPulse_Ikrum20_surrog_fitpars_perpix_NNsmoothingON.dat
"S29_PostJSI_1000V_TestPulse_Ikrum50_surrog_fitpars_perpix_NNsmoothingON.dat",

"S30_BeforeIRRAD_Coarse64_12032015_200V_SpidrTime_surrog_fitpars_perpix_NNsmoothingON.dat",
#S30_PostIRRAD_600V_TestPulse_25jan2016_surrog_fitpars_perpix_NNsmoothingON.dat,
"S30_postIrrad_800V_27092015_surrog_fitpars_perpix_NNsmoothingON.dat",
]
devices = [6, 8, 16, 17, 21, 22, 27, 29, 30]

def make_data_dict():
    paired_data = list(zip(datafiles[::2], datafiles[1::2]))
    sensor_data = {}
    for before, post in paired_data:
        sensor = before.split('_')[0]
        df_b = pd.read_csv(data_folder/before, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
        df_p = pd.read_csv(data_folder/post, skiprows=3, names=['col','row', 'p0', 'p1', 'c', 't'], sep=" ")
        sensor_data[sensor] = (df_b, df_p)


    return sensor_data

# %%
myscore = lambda x : np.abs(x-np.mean(x))/np.std(x)


def make_sur_map(df):
    sur_mat = np.zeros((256,256,4))
    for i, row in df.iterrows():
        sur_mat[int(row['col']),int(row['row'])] = [row['p0'],row['p1'],row['c'],row['t']]
    return sur_mat

def make_bin_map(df):
    bin_mat = np.zeros((256,256), dtype=np.int)
    for i, row in df.iterrows():
        bin_mat[int(row['col']),int(row['row'])] = row['bin']
    return bin_mat

def make_sur(p0, p1, c, t):
    return lambda x : p0 + p1*x - c/(x-t)


# %%
myscore = lambda x : np.abs(x-np.mean(x))/np.std(x)


# %%
def cut_range(df):
    p0_range=(-10,10)
    p1_range=(0,0.010)
    c_range=(0,15000)
    t_range=(-2000,3000)
    df=df[df['p0'].between(*p0_range) & df['p1'].between(*p1_range) & df['c'].between(*c_range) & df['t'].between(*t_range)]
    return df

def clean_parameters(df):
    df = cut_range(df)
    pre2 = df[['col','row','p0','p1','c','t']][(myscore(df[['p0','p1','c','t']]) < 3).all(axis=1)]
    return pre2


# %%
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.stats import crystalball, norm, skewnorm
from collections import OrderedDict

class NoPcaSuroGen:
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self):
        self.dist = crystalball
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()
        self.scaler = StandardScaler()
        
        
    def transform(self, data):
        data = self.scaler.transform(data)
        return data
        
    def inverse_transform(self, data):
        data = self.scaler.inverse_transform(data)
        return data
        
    @staticmethod
    def prepare_data(data):
        pre_params = data[NoPcaSuroGen._columns]
        pre_cut_bin = cut_range(pre_params)
        return pre_cut_bin

    def _generate(self, size):
        arr = np.zeros((size, len(self._columns)))
        for j, col in enumerate(self._columns):
            arr[:,j] = self.fitted_gens[col](size)
        return arr
        
    def generate(self, size):
        arr = self._generate(size)
        generated = self.inverse_transform(arr)
        return generated
        
    def _fit(self, data):
        for j, col in enumerate(self._columns):
            d = data[:,j]
            d = d[~np.isnan(d)]
            p = self.dist.fit(d)
            self.fit_params[col] = p
            self.fitted_gens[col] = lambda x=1 : self.dist.rvs(*p,x)
        
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        self._fit(data)
            
    def means(self):
        mean_arr = [self.dist.mean(*params) for params in self.fit_params.values()]
        mean_tran= self.inverse_transform([mean_arr])
        return mean_tran

    
class SuroGen(NoPcaSuroGen):
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self):
        #self.dist = skewnorm
        NoPcaSuroGen.__init__(self)
        self.dist = skewnorm
        self.scaler = StandardScaler()
        self.pca = PCA(whiten=True)
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()

    def transform(self, data):
        data = self.scaler.transform(data)
        data = self.pca.transform(data)
        return data

    def inverse_transform(self, data):
        data = self.pca.inverse_transform(data)
        data = self.scaler.inverse_transform(data)
        return data
    
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        decor = self.pca.fit_transform(data)
        self._fit(decor)
    
    def generate_decor(self, size):
        return NoPcaSuroGen._generate(self, size)

    def generate(self, size):
        arr = self.generate_decor(size)
        generated = self.inverse_transform(arr)
        return generated
    
        
    def std(self):
        mean_arr = [self.dist.std(*params) for params in self.fit_params.values()]
        #mean_tran= self.inverse_transform([mean_arr])
        return mean_arr
    
class SuroGen2(SuroGen):
    
    _columns = ['p0','p1','c','t']
    
    def __init__(self,pca):
        SuroGen.__init__(self)
        self.dist = skewnorm
        self.pca = pca
        self.fit_params = OrderedDict()
        self.fitted_gens = OrderedDict()

    
    def fit_alteration(total_data):
        pre_cut_bin = SuroGen2.prepare_data(total_data)
        scaler = StandardScaler()
        pca = PCA(whiten=True)
        data = scaler.fit_transform(pre_cut_bin)
        decor = pca.fit_transform(data)
        return scaler, pca
    
    def fit(self, grouped_data):
        pre_cut_bin = self.prepare_data(grouped_data)
        data = self.scaler.fit_transform(pre_cut_bin)
        decor = self.pca.transform(data)
        self._fit(decor)

# %%
